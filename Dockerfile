FROM node:latest

WORKDIR /app

COPY package*.json ./


RUN npm install

COPY . .

USER otrousuario

CMD [ "npm", "start" ]

#o tambien 
#ENTRYPOINT ["node", "/root/node_modules/.bin/http-server" , "./dist/"]
