
#Esta primer parte Terraford lo va sobre escribir 


provider "aws" {
   access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-west-2"
}


data "aws_ami" "my-ec2" {
  most_recent = true

  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
  owners = ["amazon"]
}


resource "aws_instance" "my-ec2" {
	# get Amazon Linux 2 AMI
    ami = data.aws_ami.amazon-2.id
    instance_type = "t3a.nano"
	
	tags = {
		Name = "My first EC2 using Terraform"

}
resource "aws_ebs_volume" "my-ec2" {
availability_zone = var.availability_zone
size = 20
}

resource "aws_security_group" "my-ec2" {
	name = "terraform-tcp-security-group"
	
	ingress {
		from_port = 80
		to_port = 80
		protocol = "tcp"
		cidr_blocks = ["11.22.33.44/0"]
	}
 
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["11.22.33.44/0"]
    }
}
